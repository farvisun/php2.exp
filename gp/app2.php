<?php

// $text = 'Welcome to PHP! Do you like PHP?';
$text = '<a href="#">Link</a>';

$a = htmlentities($text);

$b = html_entity_decode($a);

echo '<p>Original text: '.$text.'</p>';
echo '<p>After htmlentities: '.$a.'</p>';
echo '<p>After html_entity_decode: '.$b.'</p>';

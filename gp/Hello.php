<?php

echo '<h2>Please Enter variables name and color at address bar with ?</h2>';

echo '<h3 style="font-weight: bold">For Example: ?name=Ali&color=red</h3>';

echo '<pre>';
echo print_r($_GET);
echo '</pre>';

if(isset($_GET['name'])){
    $name = $_GET['name'];
}else{
    $name = 'Friend';
}

if(isset($_GET['color'])){
    $color = $_GET['color'];
}else{
    $color = 'red';
}

echo '<span style="color: '.$color.';">';
echo 'Hello '.$name.'!';
echo '</span>';
echo '<br/>';

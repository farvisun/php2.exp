<?php
function my_autoload($class){
    $classPath = "$class.php";
    if(file_exists($classPath) and is_readable($classPath)){
        include $classPath;
    }
}

spl_autoload_register('my_autoload');
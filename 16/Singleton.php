<?php

class Airplane {
    public $flightAltitude;
    private static $instance = null;
    private function __construct($flightAltitude){
        $this->flightAltitude = $flightAltitude;
    }
    public function setFlightAltitude($flightAltitude): void
    {
        $this->flightAltitude = $flightAltitude;
    }

    public function getFlightAltitude()
    {
        return $this->flightAltitude;
    }


    public static function getInstance() : Airplane {
        if(self::$instance == null){
            $instance = self::$instance = new Airplane(1,2,3,4,5);
        }else{
            $instance = self::$instance;
        }
        return $instance;
    }

}

$ap2 = Airplane::getInstance();
var_dump($ap2);
$ap2->setFlightAltitude(10000);

$ap3 = Airplane::getInstance();
var_dump($ap3);


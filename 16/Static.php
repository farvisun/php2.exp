<?php

Class Car{
    public $name;
    private static $count = 0;
    public function __construct($name)
    {
        $this->name = $name;
        self::$count = self::$count +1  ;
        echo "New $name created ! (". self::$count ." cars created!) <br>";
    }

    public static function getCount()
    {
        return self::$count;
    }

    public static function setCount(int $count): void
    {
        self::$count = $count;
    }
}


new Car("Pride");
$bmw1 = new Car("BMW1");
$bmw1::setCount(500);
new Car("Benz");
new Car("BMW2");



echo Car::getCount();

//echo $user::find(3)->filter("old")->slice(1,5)->last();





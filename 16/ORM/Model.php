<?php
include "DBConnection.php";

class Model{
    public static $table = null ;
    public static $primay_key = 'id';
    protected $conn;

    public function __construct()
    {
        $this->conn = DBConnection::getInstance();
    }

    public function all(){
        return $this->conn->query("SELECT * FROM ". static::$table);
    }
    public function find($id){
        $result = $this->conn->query("SELECT * FROM ". static::$table . " where ". static::$primay_key . " = '$id'");
        return ((Array)$result)[0];
    }
    public function remove($id){
        return (boolean) $this->conn->query("DELETE FROM ". static::$table . " where ". static::$primay_key . " = '$id'");
    }

}

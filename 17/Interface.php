<?php

interface Exportable {
    public function export($type);
}

interface I {
    const PI = 3.14;
    public function f(int $a1,int $b);
    public function g();
}
interface J extends I{
    public function j();
}
abstract class A {
    public final function t(){
        echo "from t()";
    }
}

class B extends A implements I,J{
    public function f(int $a1, int $b)
    {
    }
    public function g()
    {

    }
    public function j()
    {
    }
}



/*$ac = new A();
echo $ac->f(1,2);*/

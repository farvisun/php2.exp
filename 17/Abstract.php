<?php
abstract class A {

    public abstract function f(int $a1,int $b);
    public final function g(){
        echo "from g()";
    }
}

class B extends A{
    public function f(int $a1, int $b)
    {
        // TODO: Implement f() method.
    }
}
abstract class C extends A{

}

/*$ac = new A();
echo $ac->f(1,2);*/

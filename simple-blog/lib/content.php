<?php

function getPost($postID = null){
    if(is_null($postID) or !is_numeric($postID))
        return false;
    global $allPosts;
    foreach ($allPosts as $p){
        if($p['id'] == $postID){
            return $p;
        }
    }
    return false;
}

function get_excerpt($post){
    $postBody = $post['body'];
    $postSegments = explode('<!--more-->',$postBody);
    $moreLink = "<p class='moreLinkP'><a href='single.php?post={$post['id']}'> ادامه مطلب ...</a></p>";
    $excerpt = $postSegments[0];
    if(sizeof($postSegments) > 1)
        $excerpt .= $moreLink;
    return $excerpt;
}

function getPosts($posts,$filter = 'all',$user = ''){
    // 3 bellow lines : optional
    $filterWhitelist = ['author','oldest','all'];
    if(!in_array($filter,$filterWhitelist))
        return $posts;

    $finalPosts = $posts;
    switch ($filter){
        case 'author':
            $author = $_GET['author'];
            $finalPosts = getAuthorPosts($posts,$author);
            break;
            case 'category':

            break;
    }


    return $finalPosts;
}

function getAuthorPosts($posts,$author,$order = 'latest'){
    $authorPosts = [];
    foreach ($posts as $p){
        if($p['author'] == $author){
            $authorPosts[] = $p;
        }
    }
    return $authorPosts;
}


function getComments($postID = null){
    if(is_null($postID) or !is_numeric($postID))
        return false;
    global $allComments;
    $postComments = [];
    foreach ($allComments as $comment){
        if($comment['post_id'] == $postID){
            $postComments[] = $comment;
        }
    }
    return $postComments;
}

<?php

$filter = $_GET['filter'] ?? 'all';
$posts = getPosts($allPosts,$filter);

//$posts = array_reverse($posts); // display order

//var_dump($posts[-1]); // not available in php
$postCount = sizeof($posts) ;
$pageCount = ceil($postCount / POSTS_PER_PAGE ) ;
$currentPage = 1;
if(!empty($_GET['page']) and is_numeric($_GET['page'])){
    $currentPage = $_GET['page'];
}

$start_index = POSTS_PER_PAGE * ($currentPage - 1);
$posts = array_slice($posts,$start_index,POSTS_PER_PAGE);

$isLogin = isLogin();

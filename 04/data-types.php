<?php
$age = 29;
$Age = 29;
$name = "Loghman";
$url = "www.7Learn.com";
$pi = 3.14;
$single = false;
$numbers1 = [11,3,5,67,7,5];
$numbers2 = array(11,3,5,67,7,5);
$arr2D = [
    [1.12,2,"ali"],
    [1,array(5,6,7),3],
    false
];
$money = NULL;
$for = 1;

/*
Rules for PHP variables:
A variable starts with the $ sign, followed by the name of the variable
A variable name must start with a letter or the underscore character
A variable name cannot start with a number
A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
Variable names are case-sensitive ($age and $AGE are two different variables)
*/

$sq = 'im {$age} years old!<br>';

$dq = "im {$age}years old!<br>";

$heredoc = <<<SQL
    im $age years old!<br>
SQL;

echo "My Name is $name , im $age years old!";
echo "My Name is".$name." , im ".$age." years old!";

echo $sq;
echo $dq;
echo $heredoc;



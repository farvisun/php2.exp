<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BaseTemplate</title>
    <link rel="stylesheet" href="../milligram/css/normalize.css">
    <link rel="stylesheet" href="../milligram/css/milligram-rtl.css">
    <link rel="stylesheet" href="../milligram/css/custom.css">
    <script src="../milligram/js/jquery.min.js"></script>
</head>
<body>

<form action="auth.php?action=login" method="post">
    <div>
        <input type="text" name="username" id="username" value="Loghman" >
        <input type="password" name="password" value="">
    </div>
    <input type="submit" value="Login" id="loginSubmit">
</form>

<button id="chng">ChangeUsername</button>


<script>
    $("button#chng").click(function () {
        var un = $("#username").val();
        $("#loginSubmit").val(un + ", Please Click Here ...")
    });

</script>

</body>
</html>
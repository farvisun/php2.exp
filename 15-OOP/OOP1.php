<?php
error_reporting(E_ALL);
ini_set('display_errors',1);


class Vehicle{
    public $name;
    public $color;
    public $fuel;
    private $chassisNumber ;
    private $maxSpeed;

    public function __construct($name,$color,$fuel,$maxSpeed)
    {
        $this->name = $name;
        $this->color = $color;
        $this->fuel = $fuel;
        $this->chassisNumber = $this->generateChassisNumber();
        $this->maxSpeed = $maxSpeed;
        echo "$this->name Created!<br>";
    }

    public function start(){
        echo "$this->name started !<br>";
    }
    public function run($speed){
        if($speed <= $this->maxSpeed){
            echo "$this->name running at $speed km/h<br>";
        }else{
            echo "$this->name running at $this->maxSpeed km/h<br>";
        }
    }
/*    public function __destruct()
    {
        echo "$this->name Destructed<br>";
        unset($this->conn);
    }*/

    public function getChassisNumber(){
        return $this->chassisNumber;
    }

    public function setChassisNumber($cn){
        if(currentUserHassAccess('changeCN')) {
            $this->chassisNumber = $cn;
        }else{
            echo "access denied !<br>";
        }
    }

    private function generateChassisNumber(){
        return rand(9999,999999999);
    }

}

$car = new Vehicle("Pride","White","gas",180);
$car->start();
$car->run(200);


$airplane = new Vehicle("Airbus320","White","BenzinHavapeima",1200);
$airplane->start();
$airplane->run(500);


$f = function (){
    echo "Hi";
};
$f();


/*$car1->color = "Green";
$car1->fuel = "gas";

$car1->setChassisNumber(11111111);
echo $car1->getChassisNumber();
echo "<hr>";

$car2 = new Vehicle();
$car2->setChassisNumber(222222222);
echo $car2->getChassisNumber();*/


//$car3 = new Vehicle();

